variable "aws_account_id2" {
  type    = string
  default = "092467779203"
}

variable "aws_desired_count" {
  type    = number
  default = 1
}
variable "aws_ecs_cluster" {
  type = string
}
variable "aws_security_group" {
  type = string
}
variable "aws_subnet" {
  type = string
}
variable "branch_name" {
  type = string
}
variable "codepipeline_bucket" {
  type = string
}
variable "deploy_environment" {
  type = string
}
variable "deployment_max_percent" {
  type    = number
  default = 100
}
variable "deployment_min_healthy_percent" {
  type    = number
  default = 0
}
variable "deploy_versions" {
  type    = string
  default = true
}
variable "deploy_role" {
  default = "FDH-PowerUser-Role"
}
variable "dockerhub_user" {
  description = "used to allow more than 100 pull in 6 hours (should be 200) see https://docs.docker.com/docker-hub/download-rate-limit/"
  type        = string
}

variable "elasticsearch_co_username" {
  type = string
}
variable "elasticsearch_host" {
  type = string
}
variable "ecs_image_pull_behavior" {
  type    = string
  default = "always"
}
variable "enable_cross_account" {
  type = string
}
variable "repository_name" {
  type = string
}
variable "tag" {
  type = map(any)
  default = {
    Project = "FactoryDataHub"
  }
}
variable "retention_in_days" {
  type    = number
  default = 30
}
variable "role_arn" {
  type = string
}
variable "role_arn2" {
  type    = string
  default = "arn:aws:iam::092467779203:role/FDH-PowerUser-Role"
}
variable "role_arn_codepipeline_name" {
  type = string
}
variable "role_arn_task_name" {
  type    = string
  default = "fdh-task-role"
}
variable "s3_cache" {
  type = string
}
data "aws_caller_identity" "current" {}
data "aws_region" "current" {}
locals {
  account_id      = data.aws_caller_identity.current.account_id
  region          = data.aws_region.current.name
  repository_name = var.repository_name
  role_prefix     = "arn:aws:iam::${local.account_id}:role/"
  role_prefix2    = "arn:aws:iam::${var.aws_account_id2}:role/"
}
locals {
  proxy_name = "${local.repository_name}-proxy"
}

locals {
  arn_target            = "arn:aws:events:${local.region}:${local.account_id}:event-bus/default"
  environment           = var.deploy_environment
  image_repo            = "${local.account_id}.dkr.ecr.${local.region}.amazonaws.com/"
  image_repo_name       = local.repository_name
  key                   = "terraform/tfstate/$(local.repository_name).tfstate"
  role_arn              = "${local.role_prefix}${var.deploy_role}"
  role_arn2             = "${local.role_prefix2}${var.deploy_role}"
  role_arn_codebuild    = "${local.role_prefix}service-role/codebuild-fdh-codebuild-service-role"
  role_arn_codepipeline = "${local.role_prefix}${var.role_arn_codepipeline_name}"
  role_arn_source       = "${local.role_prefix2}AssumeCodeCommitGucciDev"
  role_arn_target       = "${local.role_prefix}fdh-start-pipeline-automation"
  role_arn_target2      = "${local.role_prefix2}fdh-start-pipeline-automation"
  role_arn_task         = "${local.role_prefix}${var.role_arn_task_name}"
  buildspec = templatefile("${path.module}/templates/buildspec.tmpl",
    {
      dockerhub_user  = var.dockerhub_user
      environment     = var.deploy_environment
      image_repo      = local.image_repo
      image_repo_name = local.image_repo_name
      proxy_name      = local.proxy_name
    }
  )
  ecr-repository-list          = [local.repository_name]
  ecr-repository-list-snapshot = [for name in local.ecr-repository-list : "${name}-snapshot"]
  deploy2_name                 = "${local.repository_name}-deploy"
  deployspec = templatefile("${path.module}/templates/deployspec.tmpl",
    {
      aws_desired_count              = var.aws_desired_count
      aws_ecs_cluster                = var.aws_ecs_cluster
      aws_security_group             = var.aws_security_group
      aws_service_name               = local.repository_name
      aws_stream_prefix              = local.repository_name
      aws_subnet                     = var.aws_subnet
      deployment_max_percent         = var.deployment_max_percent
      deployment_min_healthy_percent = var.deployment_min_healthy_percent
      ecs_image_pull_behavior        = var.ecs_image_pull_behavior
      elasticsearch_co_username      = var.elasticsearch_co_username
      elasticsearch_host             = var.elasticsearch_host
      image_repo                     = local.image_repo
      image_repo_name                = local.repository_name
      proxy_name                     = local.proxy_name
      task_role_arn                  = local.role_arn_task
    }
  )
}
