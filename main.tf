terraform {
  backend "s3" {
  }
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.9.0"
    }
  }
  required_version = ">= 0.15.1, < 2.0.0"

}
provider "aws" {
  assume_role {
    role_arn = var.role_arn
  }
}
provider "aws" {
  alias = "prod"
  assume_role {
    role_arn = var.role_arn2
  }
}
