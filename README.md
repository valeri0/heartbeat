## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 0.15.1, < 2.0.0 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | ~> 3.9.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | ~> 3.9.0 |

## Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_deploy"></a> [deploy](#module\_deploy) | git::https://bitbucket.org/valeri0/deploy_x_application | 0.4.0 |
| <a name="module_ecr_immutable"></a> [ecr\_immutable](#module\_ecr\_immutable) | git::https://bitbucket.org/valeri0/ecr.git | 0.2.0 |
| <a name="module_ecr_mutable"></a> [ecr\_mutable](#module\_ecr\_mutable) | git::https://bitbucket.org/valeri0/ecr.git | 0.2.0 |

## Resources

| Name | Type |
|------|------|
| [aws_cloudwatch_log_group.log](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cloudwatch_log_group) | resource |
| [aws_caller_identity.current](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/caller_identity) | data source |
| [aws_region.current](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/region) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_aws_account_id2"></a> [aws\_account\_id2](#input\_aws\_account\_id2) | n/a | `string` | `"092467779203"` | no |
| <a name="input_aws_desired_count"></a> [aws\_desired\_count](#input\_aws\_desired\_count) | n/a | `number` | `1` | no |
| <a name="input_aws_ecs_cluster"></a> [aws\_ecs\_cluster](#input\_aws\_ecs\_cluster) | n/a | `string` | n/a | yes |
| <a name="input_aws_security_group"></a> [aws\_security\_group](#input\_aws\_security\_group) | n/a | `string` | n/a | yes |
| <a name="input_aws_subnet"></a> [aws\_subnet](#input\_aws\_subnet) | n/a | `string` | n/a | yes |
| <a name="input_branch_name"></a> [branch\_name](#input\_branch\_name) | n/a | `string` | n/a | yes |
| <a name="input_codepipeline_bucket"></a> [codepipeline\_bucket](#input\_codepipeline\_bucket) | n/a | `string` | n/a | yes |
| <a name="input_deploy_environment"></a> [deploy\_environment](#input\_deploy\_environment) | n/a | `string` | n/a | yes |
| <a name="input_deploy_role"></a> [deploy\_role](#input\_deploy\_role) | n/a | `string` | `"FDH-PowerUser-Role"` | no |
| <a name="input_deploy_versions"></a> [deploy\_versions](#input\_deploy\_versions) | n/a | `string` | `true` | no |
| <a name="input_deployment_max_percent"></a> [deployment\_max\_percent](#input\_deployment\_max\_percent) | n/a | `number` | `100` | no |
| <a name="input_deployment_min_healthy_percent"></a> [deployment\_min\_healthy\_percent](#input\_deployment\_min\_healthy\_percent) | n/a | `number` | `0` | no |
| <a name="input_dockerhub_user"></a> [dockerhub\_user](#input\_dockerhub\_user) | used to allow more than 100 pull in 6 hours (should be 200) see https://docs.docker.com/docker-hub/download-rate-limit/ | `string` | n/a | yes |
| <a name="input_ecs_image_pull_behavior"></a> [ecs\_image\_pull\_behavior](#input\_ecs\_image\_pull\_behavior) | n/a | `string` | `"always"` | no |
| <a name="input_elasticsearch_co_username"></a> [elasticsearch\_co\_username](#input\_elasticsearch\_co\_username) | n/a | `string` | n/a | yes |
| <a name="input_elasticsearch_host"></a> [elasticsearch\_host](#input\_elasticsearch\_host) | n/a | `string` | n/a | yes |
| <a name="input_enable_cross_account"></a> [enable\_cross\_account](#input\_enable\_cross\_account) | n/a | `string` | n/a | yes |
| <a name="input_repository_name"></a> [repository\_name](#input\_repository\_name) | n/a | `string` | n/a | yes |
| <a name="input_retention_in_days"></a> [retention\_in\_days](#input\_retention\_in\_days) | n/a | `number` | `30` | no |
| <a name="input_role_arn"></a> [role\_arn](#input\_role\_arn) | n/a | `string` | n/a | yes |
| <a name="input_role_arn2"></a> [role\_arn2](#input\_role\_arn2) | n/a | `string` | `"arn:aws:iam::092467779203:role/FDH-PowerUser-Role"` | no |
| <a name="input_role_arn_codepipeline_name"></a> [role\_arn\_codepipeline\_name](#input\_role\_arn\_codepipeline\_name) | n/a | `string` | n/a | yes |
| <a name="input_role_arn_task_name"></a> [role\_arn\_task\_name](#input\_role\_arn\_task\_name) | n/a | `string` | `"fdh-task-role"` | no |
| <a name="input_s3_cache"></a> [s3\_cache](#input\_s3\_cache) | n/a | `string` | n/a | yes |
| <a name="input_tag"></a> [tag](#input\_tag) | n/a | `map(any)` | <pre>{<br>  "Project": "FactoryDataHub"<br>}</pre> | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_role_arn"></a> [role\_arn](#output\_role\_arn) | n/a |
