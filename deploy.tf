module "ecr_immutable" {
  image_tag_mutability = "IMMUTABLE"
  name                 = local.ecr-repository-list
  policy               = <<EOF
{
    "rules": [
        {
            "rulePriority": 1,
            "description": "Keep no more than 5 images ",
            "selection": {
                "countNumber": 5,
                "countType": "imageCountMoreThan",
                "tagStatus": "any"
            },
            "action": {
                "type": "expire"
            }
        }    
      ]
}
EOF

  #source = "/Users/vcrini/Repositories/terraform-modules/ecr"
  source = "git::https://bitbucket.org/valeri0/ecr.git?ref=0.2.0"
  tags   = var.tag
}
module "ecr_mutable" {
  name   = local.ecr-repository-list-snapshot
  policy = <<EOF
{
    "rules": [
        {
            "rulePriority": 1,
            "description": "Keep last 3 images",
            "selection": {
                "tagStatus": "any",
                "countType": "imageCountMoreThan",
                "countNumber": 3
            },
            "action": {
                "type": "expire"
            }
        }    
      ]
}
EOF
  source = "git::https://bitbucket.org/valeri0/ecr.git?ref=0.2.0"
  tags   = var.tag
}

module "deploy" {
  #source                  = "/Users/vcrini/Repositories/terraform-modules/deploy_x_application"
  branch_name             = var.branch_name
  buildspec               = local.buildspec
  cluster_name            = var.aws_ecs_cluster
  codepipeline_bucket     = var.codepipeline_bucket
  deploy_environment      = var.deploy_environment
  deployspec              = local.deployspec
  poll_for_source_changes = "false"
  repository_name         = local.repository_name
  role_arn                = local.role_arn
  role_arn_codebuild      = local.role_arn_codebuild
  role_arn_codepipeline   = local.role_arn_codepipeline
  role_arn_source         = local.role_arn_source
  s3_cache                = var.s3_cache
  source                  = "git::https://bitbucket.org/valeri0/deploy_x_application?ref=0.4.0"
  tags                    = var.tag
  vm_depends_on           = [aws_cloudwatch_log_group.log]
}
resource "aws_cloudwatch_log_group" "log" {
  for_each          = toset([module.deploy.cloudwatch_build_log, module.deploy.cloudwatch_deploy_log])
  name              = each.value
  retention_in_days = var.retention_in_days
  tags              = var.tag
}
